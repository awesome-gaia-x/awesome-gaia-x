# Awesome Gaia-X

A curated list of various Gaia-X resources.

## Contents

- [Implementations](#implementations)


## GXFS Implementations

Mapping from the [specifications](https://www.gxfs.eu/specifications/) to GXFS reference implementations or deliverables. 

### [Authentication / Authorisation](https://www.gxfs.eu/authentication-authorisation/)
- https://gitlab.com/gaia-x/data-infrastructure-federation-services/authenticationauthorization


### [Personal Credential Manager](https://www.gxfs.eu/personal-credential-manager/)
- https://gitlab.com/gaia-x/data-infrastructure-federation-services/pcm


### [Organization Credential Manager](https://www.gxfs.eu/organizational-credential-manager/)
- https://gitlab.com/gaia-x/data-infrastructure-federation-services/ocm


### [Trust Services API](https://www.gxfs.eu/trust-services-api/)
- https://gitlab.com/gaia-x/data-infrastructure-federation-services/tsa


### [Federated Catalogue](https://www.gxfs.eu/core-catalogue-features/)
- https://gitlab.com/gaia-x/data-infrastructure-federation-services/cat


### [Data Contract Service](https://www.gxfs.eu/data-contract-service/)
- https://gitlab.com/gaia-x/data-infrastructure-federation-services/dct/dct


### [Data Exchange Logging Service](https://www.gxfs.eu/data-exchange-logging-service/)
- https://gitlab.com/gaia-x/data-infrastructure-federation-services/del/del

### [Continuous Automated Monitoring](https://www.gxfs.eu/notarization-api/)
- https://gitlab.com/gaia-x/data-infrastructure-federation-services/cam


### [Notarization API](https://www.gxfs.eu/continuous-automated-monitoring/)
- https://gitlab.com/gaia-x/data-infrastructure-federation-services/not


### [Portal](https://www.gxfs.eu/portal/)
- https://gitlab.com/gaia-x/data-infrastructure-federation-services/por


### [Orchestration](https://www.gxfs.eu/orchestration/)
- https://gitlab.com/gaia-x/data-infrastructure-federation-services/orc

### [IDM & Trust Architecture](https://www.gxfs.eu/idm-trust-architecture/)
- ?


## Documentation










